# Clean Harbors CMS

Clean Harbors CMS is Born Group's base Drupal profile for Acquia Cloud Site Factory.  This is a sub-Profile of the stable and recommended [*lightning*](https://github.com/acquia/lightning) profile.   We are using a sub-profile so we can make our own customizations and still rely upon the profile.

## Table of Contents

* [Getting Started](#getting-started)
* [Get Code](#get-code)
* [Working With BLT](#working-with-blt)
* [Resources](#resources)

## Getting Started

This project is based on BLT, an open-source project template and tool that enables building, testing, and deploying Drupal installations following Acquia Professional Services best practices. While this is one of many methodologies, it is our recommended methodology. 

* Review the [Required / Recommended Skills](http://blt.readthedocs.io/en/latest/readme/skills) for working with a BLT project
* Ensure that your computer meets the minimum installation requirements (and then install the required applications). See the [System Requirements](http://blt.readthedocs.io/en/latest/INSTALL/#system-requirements) for more information.
* Request access to Born Group organization for the project repo in Bitbucket (if needed).
* Request access to the Acquia Cloud Environment for your project (if needed)
* Setup a SSH key that can be used for GitHub and the Acquia Cloud (you CAN use the same key)
    * [Setup GitHub SSH Keys](https://help.github.com/articles/adding-a-new-ssh-key-to-your-github-account/)
    * [Setup Acquia Cloud SSH Keys](https://docs.acquia.com/acquia-cloud/ssh/generate)

## Get Code

```shell
$ git clone git@bitbucket.org:borngroup/clean-harbors-cms.git
```
Change directories to the new cloned repo.
```shell
$ cd clean-harbors-cms
```
Install Composer Dependencies (warning: this can take some time based on internet speeds)
```shell
$ composer install
```

## Setup Local Environment 

### Drupal VM (Mac or Windows) - if you are using Drupal VM:

BLT has a script to create and boot a VM.
```shell
$ blt vm

    a) Drupal VM is not currently installed. Install it now? (y/n) `y`

    b) Which base box would you like to use? `0`

    c) Do you want to boot Drupal VM? (y/n) `y`

```

Once the VM is loaded, you can visit your VM at http://dashboard.local.cleanprofile.com/ and also make sure your site loads locally at http://local.cleanprofile.com/.

Then, you use the following command to develop and commit code from the VM instance.  This is where you also execute BLT commands.
```shell
$ vagrant ssh
```

### Linux
Configure your Apache server and local Hosts file to load your site.  For a basic tutorial see https://www.digitalocean.com/community/tutorials/how-to-set-up-apache-virtual-hosts-on-ubuntu-14-04-lts.

## Configure Drupal / Resetting Drupal
```shell
$ blt setup

  a) You are about to DROP all tables in your 'drupal' database. Do you want to continue? (yes/no) [yes]: `yes`
```

Access the site and do necessary work at \<project local url> by running
```shell
$ drush uli
```
Use this URL in your brower to reset the Admin password so you can log in locally.

### Connect your VM to Acquia

BLT 9 and Drush 9 require all blt and drush commands to be executed inside of the VM. Because of this requirement, the VM must have SSH access to Acquia.

```shell
$ cd ~/.ssh
$ ssh-keygen -b 4096
$ cat ~/.ssh/id_rsa.pub
```
The public key needs to be added to your Acquia Cloud account.  Once your key is added to Acquia, you can use Drush to list the site aliases.
```shell
$ drush sa                           # This will list your aliases

$ drush @cleanprofile.01test status  # This will list a status of the site on the Test Server.
```

## Working With BLT

Additional [BLT documentation](http://blt.readthedocs.io) may be useful. You may also access a list of BLT commands by running

```shell
$ blt
```

Note the following properties of this project:
* Primary development branch: \develop
* Local environment: local
* Local site URL: http://local.bornfresh.com

## Working With a BLT Project

BLT projects are designed to instill software development best practices (including git workflows). 

Our BLT Developer documentation includes an (example workflow)[http://blt.readthedocs.io/en/latest/readme/dev-workflow/#workflow-example-local-development].

### Important Configuration Files

BLT uses a number of configuration (.yml or .json) files to define and customize behaviors. Some examples of these are:

* blt/blt.yml (formerly blt/project.yml prior to BLT 9.x)
* blt/local.blt.yml
* box/config.yml (if using Drupal VM)
* drush/sites (contains Drush aliases for this project)
* composer.json (includes required components, including Drupal Modules, for this project)

## Resources

* JIRA - \<provide link>
* Bitbucket - https://bitbucket.org/borngroup/clean-harbors-cms
* Acquia Cloud subscription - https://accounts.acquia.com/sign-in?site=cloud&path=app/develop/applications/7764ad22-2603-434c-a2ea-01a9b5dfc24d&session-expire=1
